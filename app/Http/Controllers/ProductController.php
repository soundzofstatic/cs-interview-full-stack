<?php

namespace App\Http\Controllers;

use App\Helpers\Dpaz\SymCrypt;
use App\Http\Requests\OrderHistoryStoreRequest;
use App\Http\Requests\ProductInsertRequest;
use App\Http\Requests\ProductInventoryInsertRequest;
use App\Http\Requests\ProductUpdateRequest;
use App\Models\Inventory;
use App\Models\Order;
use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {

            if(!Auth::check()) {

                throw new \Exception('User Authentication required.');

            }

            $user = Auth::user();

            $products = Product::with(['skus'])
                ->where('admin_id', '=', $user->id)
                ->paginate(25);

            return view('dashboard.products.home')
                ->with(compact([
                    'user',
                    'products',
                ]));

        } catch (\Exception $e) {

            return redirect()->back()->withErrors(['errors' => $e->getMessage()]);

        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        try {

            if(!Auth::check()) {

                throw new \Exception('User Authentication required.');

            }

            $user = Auth::user();

            return view('dashboard.products.create')
                ->with(compact([
                    'user'
                ]));

        } catch (\Exception $e) {

            return redirect()->back()->withErrors(['errors' => $e->getMessage()]);

        }
    }

    /**
     * Method used to display a form used to create Inventory in the context of a Product
     *
     * @param Product $product
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\RedirectResponse
     */
    public function createInventory(Product $product)
    {
        try {

            if(!Auth::check()) {

                throw new \Exception('User Authentication required.');

            }

            $user = Auth::user();

            return view('dashboard.products.create-product-inventory')
                ->with(compact([
                    'user',
                    'product'
                ]));

        } catch (\Exception $e) {

            return redirect()->back()->withErrors(['errors' => $e->getMessage()]);

        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProductInsertRequest $request)
    {
        try {

            $product = new Product();
            $product->name = $request->name;
            $product->admin_id = Auth::user()->id;
            $product->description = $request->description;
            $product->style = $request->style;
            $product->brand = $request->brand;
            $product->type = $request->type;
            $product->shipping_price = $this->removeUsdFormatStyling($request->shipping_price) * 100; // Change to USD cents
            $product->note = empty($request->note) ? null : $request->note;
            $product->url = empty($request->url) ? null : $request->url;
            $product->save();

            return redirect()
                ->route('dashboard.product.show', ['product' => $product->id])
                ->with('success', 'Successfully created product.');

        } catch (\Exception $e) {

            return redirect()->back()->withErrors(['errors' => $e->getMessage()]);

        }
    }

    /**
     * Method used to store inventory in the context of a given Product
     *
     * @param ProductInventoryInsertRequest $request
     * @param Product $product
     * @return \Illuminate\Http\RedirectResponse
     */
    public function storeInventory(ProductInventoryInsertRequest $request, Product $product)
    {
        try {

            $inventory = new Inventory();
            $inventory->product_id = $product->id;
            $inventory->sku = $request->sku;
            $inventory->quantity = $request->quantity;
            $inventory->color = $request->color;
            $inventory->size = $request->size;
            $inventory->weight = $request->weight;
            $inventory->price_cents = $this->removeUsdFormatStyling($request->price) * 100;
            $inventory->sale_price_cents = $this->removeUsdFormatStyling($request->sale_price) * 100;
            $inventory->cost_cents = $this->removeUsdFormatStyling($request->cost) * 100;
            $inventory->length = $request->length;
            $inventory->width = $request->width;
            $inventory->height = $request->height;
            $inventory->note = empty($request->note) ? null : $request->note;
            $inventory->save();

            return redirect()
                ->route('dashboard.product.show', ['product' => $product->id])
                ->with('success', 'Successfully created inventory for product.');

        } catch (\Exception $e) {

            return redirect()->back()->withErrors(['errors' => $e->getMessage()]);

        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product)
    {
        try {

            if(!Auth::check()) {

                throw new \Exception('User Authentication required.');

            }

            $user = Auth::user();

            return view('dashboard.products.show')
                ->with(compact([
                    'user',
                    'product',
                ]));

        } catch (\Exception $e) {

            return redirect()->back()->withErrors(['errors' => $e->getMessage()]);

        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product)
    {
        try {

            if(!Auth::check()) {

                throw new \Exception('User Authentication required.');

            }

            $user = Auth::user();

            return view('dashboard.products.edit')
                ->with(compact([
                    'user',
                    'product',
                ]));

        } catch (\Exception $e) {

            return redirect()->back()->withErrors(['errors' => $e->getMessage()]);

        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(ProductUpdateRequest $request, Product $product)
    {
        try {

            $product->name = $request->name;
            $product->description = $request->description;
            $product->style = $request->style;
            $product->brand = $request->brand;
            $product->type = $request->type;
            $product->shipping_price = $this->removeUsdFormatStyling($request->shipping_price) * 100; // Change to USD cents
            $product->note = empty($request->note) ? null : $request->note;
            $product->url = empty($request->url) ? null : $request->url;
            $product->save();

            return redirect()
                ->route('dashboard.product.show', ['product' => $product->id])
                ->with('success', 'Successfully updated product.');

        } catch (\Exception $e) {

            return redirect()->back()->withErrors(['errors' => $e->getMessage()]);

        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product)
    {
        try {

            $user = Auth::user();

            // todo - Validate that User is Auth
            // todo - validate that Auth user can delete product

            $product->delete();

            return redirect()->route('dashboard.product.home')
                ->with('success', 'Successfully deleted product.');

        } catch (\Exception $e) {

            Log::error($e);
            return redirect()->back()->withErrors(['errors' => $e->getMessage()]);

        }
    }

    // todo - This should be a global utility
    private function removeUsdFormatStyling($amountStringValue)
    {

        return empty($amountStringValue) ? '0.00' : preg_replace('/[^0-9\.]/', '', $amountStringValue);

    }
}
