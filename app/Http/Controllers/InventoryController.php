<?php

namespace App\Http\Controllers;

use App\Http\Requests\InventoryUpdateRequest;
use App\Models\Inventory;
use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Routing\Route;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Session;

class InventoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($reset = null)
    {
        try {

            if(!Auth::check()) {

                throw new \Exception('User Authentication required.');

            }

            if(!empty($reset) AND $reset == 'reset') {

                Session()->forget(['query', 'quantity_limit']);

            }

            $user = Auth::user();

            if(!empty(Session::get('query')) OR !empty(Session::get('quantity_limit'))) {

                $session = Session::all();

                $inventory = Inventory::with(['product'])
                    ->whereHas('product', function (Builder $query) use ($user) {
                        $query->where('admin_id', '=', $user->id);
                    })
                    ->where(function ($query) use ($user, $session) {
                        $query->where('sku', 'LIKE', '%' . $session['query'] . '%');
                        $query->orWhereHas('product', function (Builder $query) use ($user, $session) {
                            $query->where('id', '=', $session['query']);
                        });
                    })
                    ->where('quantity', '<=', $session['quantity_limit'])
                    ->paginate(50);

            } else {

                $inventory = Inventory::with(['product'])
                    ->whereHas('product', function (Builder $query) use ($user) {
                        $query->where('admin_id', '=', $user->id);
                    })
                    ->paginate(50);

            }

            $allInventory = Inventory::with(['product'])
                ->select('price_cents', 'cost_cents', 'quantity')
                ->whereHas('product', function (Builder $query) use ($user) {
                    $query->where('admin_id', '=', $user->id);
                })
                ->get();

            return view('dashboard.inventory.home')
                ->with(compact([
                    'user',
                    'inventory',
                    'allInventory'
                ]));

        } catch (\Exception $e) {

            dd($e);
            return redirect()->back()->withErrors(['errors' => $e->getMessage()]);

        }

    }

    /**
     * Method used to process a search request to filter Inventory view
     *
     * @param Request $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\RedirectResponse
     */
    public function search(Request $request)
    {
        try {

            if(!Auth::check()) {

                throw new \Exception('User Authentication required.');

            }

            $user = Auth::user();

            $request->session()->forget(['query', 'quantity_limit']);

            $inventory = Inventory::with(['product'])
                ->whereHas('product', function (Builder $query) use ($user, $request) {
                    $query->where('admin_id', '=', $user->id);
                })
                ->where(function ($query) use ($user, $request) {
                    $query->where('sku', 'LIKE', '%' . $request->input('query') . '%');
                    $query->orWhereHas('product', function (Builder $query) use ($user, $request) {
                        $query->where('id', '=', $request->input('query'));
                    });
                })
                ->where('quantity', '<=', $request->quantity_limit)
                ->paginate(50);

            $allInventory = Inventory::with(['product'])
                ->select('price_cents', 'cost_cents', 'quantity')
                ->whereHas('product', function (Builder $query) use ($user) {
                    $query->where('admin_id', '=', $user->id);
                })
                ->get();

            $request->session()->put('query', $request->input('query'));
            $request->session()->put('quantity_limit', $request->input('quantity_limit'));

            return view('dashboard.inventory.home')
                ->with(compact([
                    'user',
                    'inventory',
                    'allInventory'
                ]));

        } catch (\Exception $e) {

            return redirect()->back()->withErrors(['errors' => $e->getMessage()]);

        }

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Inventory  $inventory
     * @return \Illuminate\Http\Response
     */
    public function show(Inventory $inventory)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Inventory  $inventory
     * @return \Illuminate\Http\Response
     */
    public function edit(Inventory $inventory)
    {
        try {

            if(!Auth::check()) {

                throw new \Exception('User Authentication required.');

            }

            $user = Auth::user();

            return view('dashboard.inventory.edit')
                ->with(compact([
                    'user',
                    'inventory',
                ]));

        } catch (\Exception $e) {

            return redirect()->back()->withErrors(['errors' => $e->getMessage()]);

        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Inventory  $inventory
     * @return \Illuminate\Http\Response
     */
    public function update(InventoryUpdateRequest $request, Inventory $inventory)
    {
        try {

            $inventory->quantity = $request->quantity;
            $inventory->color = $request->color;
            $inventory->size = $request->size;
            $inventory->weight = $request->weight;
            $inventory->price_cents = $this->removeUsdFormatStyling($request->price) * 100;
            $inventory->sale_price_cents = $this->removeUsdFormatStyling($request->sale_price) * 100;
            $inventory->cost_cents = $this->removeUsdFormatStyling($request->cost) * 100;
            $inventory->length = $request->length;
            $inventory->width = $request->width;
            $inventory->height = $request->height;
            $inventory->note = empty($request->note) ? null : $request->note;
            $inventory->save();

            return redirect()
                ->route('dashboard.inventory.home')
                ->with('success', 'Successfully updated inventory.');

        } catch (\Exception $e) {

            return redirect()->back()->withErrors(['errors' => $e->getMessage()]);

        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Inventory  $inventory
     * @return \Illuminate\Http\Response
     */
    public function destroy(Inventory $inventory)
    {
        try {

            $user = Auth::user();

            // todo - Validate that User is Auth
            // todo - validate that Auth user can delete product

            $inventory->delete();

            return redirect()->route('dashboard.inventory.home')
                ->with('success', 'Successfully deleted inventory.');

        } catch (\Exception $e) {

            Log::error($e);
            return redirect()->back()->withErrors(['errors' => $e->getMessage()]);

        }
    }

    // todo - This should be a global utility
    private function removeUsdFormatStyling($amountStringValue)
    {

        return empty($amountStringValue) ? '0.00' : preg_replace('/[^0-9\.]/', '', $amountStringValue);

    }
}
