<?php

namespace App\Console\Commands;

use App\Models\User;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;

class InflateUsersData extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'inflate:data:users';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command used to insert User data into system from CSV file';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {

        $started = Carbon::now();
        echo "-- " . $this->signature . " Started at: ". $started->format('c') . PHP_EOL;
        Log::info($this->signature . " Started at: ". $started->format('c'));

        try {

            $relativeFilePath = 'data/users.csv';

            if(!Storage::disk('local')->exists($relativeFilePath)) {

                throw new \Exception('Users data file cannot be found');

            }

            $absoluteFilePath = Storage::path($relativeFilePath);
            $handle = fopen($absoluteFilePath, 'r');
            $rows = 0;
            $new = 0;
            $update = 0;

            while (($row = fgetcsv($handle, 1000, ",")) !== FALSE) {

                $rows++;

                if($rows % 100 == 0) {

                    echo '-- On row  ' . $rows . PHP_EOL;

                }

                if($rows == 1) { // Skip Header Row
                    continue;
                }

                $user = User::find($row[0]);

                if(empty($user)) {

                    $user = new User();
                    $user->id = $row[0];
                    $user->name = $row[1];
                    $user->email = $row[2];
                    $user->password = bcrypt($row[4]);
                    $user->superadmin = $row[5] == '1' ? true : false;
                    $user->shop_name = $row[6];
                    $user->shop_domain = $row[13];
                    $user->card_brand = empty($row[10]) ? null : $row[10];
                    $user->card_last_four = empty($row[11]) ? null : $row[11];
                    $user->trial_starts_at = empty($row[16]) ? null : Carbon::createFromFormat('Y-m-d H:i:s', $row[16]);
                    $user->trial_ends_at = empty($row[12]) ? null : Carbon::createFromFormat('Y-m-d H:i:s', $row[12]);
                    $user->is_enabled = $row[14] == '1' ? true : false;
                    $user->billing_plan = $row[15];
                    $user->created_at = Carbon::createFromFormat('Y-m-d H:i:s', $row[8]);
                    $user->updated_at = Carbon::createFromFormat('Y-m-d H:i:s', $row[9]);
                    $user->save();
                    $new++;

                } else {

                    // User ID already exists
                    $user->name = $row[1];
                    $user->email = $row[2];
                    $user->password = bcrypt($row[4]);
                    $user->superadmin = $row[5] == '1' ? true : false;
                    $user->shop_name = $row[6];
                    $user->shop_domain = $row[13];
                    $user->card_brand = empty($row[10]) ? null : $row[10];
                    $user->card_last_four = empty($row[11]) ? null : $row[11];
                    $user->trial_starts_at = empty($row[16]) ? null : Carbon::createFromFormat('Y-m-d H:i:s', $row[16]);
                    $user->trial_ends_at = empty($row[12]) ? null : Carbon::createFromFormat('Y-m-d H:i:s', $row[12]);
                    $user->is_enabled = $row[14] == '1' ? true : false;
                    $user->billing_plan = $row[15];
                    $user->created_at = Carbon::createFromFormat('Y-m-d H:i:s', $row[8]);
                    $user->updated_at = Carbon::createFromFormat('Y-m-d H:i:s', $row[9]);
                    $user->save();
                    $update++;
                }

            }
            fclose($handle);

            echo '-- -- Successfully inserted ' . $new . ' users and updated ' . $update . ' users.' . PHP_EOL;
            Log::info('Successfully inserted ' . $new . ' users and updated ' . $update . ' users.');

        } catch (\Exception $e) {

            echo $e->getMessage() . PHP_EOL;
            Log::critical($e);

        }

        $ended = Carbon::now();
        echo "-- " . $this->signature . " Ended at: ". $ended->format('c') . " - Duration : " . $started->diffInSeconds($ended) . " seconds" . PHP_EOL;
        Log::info($this->signature . " Ended at: ". $ended->format('c') . " - Duration : " . $started->diffInSeconds($ended) . " seconds");

    }
}
