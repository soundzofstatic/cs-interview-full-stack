<?php

namespace App\Console\Commands;

use App\Models\Order;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;

class InflateOrdersData extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'inflate:data:orders';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command used to insert Order data into system from CSV file';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $started = Carbon::now();
        echo "-- " . $this->signature . " Started at: ". $started->format('c') . PHP_EOL;
        Log::info($this->signature . " Started at: ". $started->format('c'));

        try {

            $relativeFilePath = 'data/orders.csv';

            if(!Storage::disk('local')->exists($relativeFilePath)) {

                throw new \Exception('Products data file cannot be found');

            }

            $absoluteFilePath = Storage::path($relativeFilePath);
            $handle = fopen($absoluteFilePath, 'r');
            $rows = 0;
            $new = 0;
            $update = 0;

            while (($row = fgetcsv($handle, 1000, ",")) !== FALSE) {

                $rows++;

                if($rows % 100 == 0) {

                    echo '-- -- On row  ' . $rows . PHP_EOL;

                }

                if($rows == 1) { // Skip Header Row
                    continue;
                }

                // Set string 'NULL' to null datatype
                foreach($row as $key=>$value) {

                    if(strtoupper($value) != 'NULL') {
                        continue;
                    }

                    $row[$key] = null;

                }

                $order = Order::find($row[0]);

                if(empty($order)) {

                    $order = new Order();
                    $order->id = $row[0];
                    $order->product_id = $row[1];
                    $order->inventory_id = $row[2];
                    $order->street_address = $row[3];
                    $order->apartment = $row[4];
                    $order->city = $row[5];
                    $order->state = $row[6];
                    $order->country_code = $row[7];
                    $order->zip = $row[8];
                    $order->phone_number = $row[9];
                    $order->email = $row[10];
                    $order->name = $row[11];
                    $order->order_status = $row[12];
                    $order->payment_ref = empty($row[13]) ? null : $row[13];
                    $order->transaction_id = empty($row[14]) ? null : $row[14];
                    $order->payment_amt_cents = $row[15];
                    $order->ship_charged_cents = empty($row[16]) ? null : $row[16];
                    $order->ship_cost_cents = empty($row[17]) ? null : $row[17];
                    $order->subtotal_cents = $row[18];
                    $order->total_cents = $row[19];
                    $order->shipper_name = empty($row[20]) ? null : $row[20];
                    $order->payment_date = empty($row[21]) ? null : Carbon::createFromFormat('Y-m-d H:i:s', $row[21]);
                    $order->shipped_date = empty($row[22]) ? null : Carbon::createFromFormat('Y-m-d H:i:s', $row[22]);
                    $order->tracking_number = empty($row[23]) ? null : $row[23];
                    $order->tax_total_cents = empty($row[24]) ? null : $row[24];
                    $order->created_at = Carbon::createFromFormat('Y-m-d H:i:s', $row[25]);
                    $order->updated_at = Carbon::createFromFormat('Y-m-d H:i:s', $row[26]);
                    $order->save();
                    $new++;

                } else {

                    // Order ID already exists
                    $order = new Order();
                    $order->product_id = $row[1];
                    $order->inventory_id = $row[2];
                    $order->street_address = $row[3];
                    $order->apartment = $row[4];
                    $order->city = $row[5];
                    $order->state = $row[6];
                    $order->country_code = $row[7];
                    $order->zip = $row[8];
                    $order->phone_number = $row[9];
                    $order->email = $row[10];
                    $order->name = $row[11];
                    $order->order_status = $row[12];
                    $order->payment_ref = $row[13];
                    $order->transaction_id = $row[14];
                    $order->payment_amt_cents = $row[15];
                    $order->ship_charged_cents = $row[16];
                    $order->ship_cost_cents = $row[17];
                    $order->subtotal_cents = $row[18];
                    $order->total_cents = $row[19];
                    $order->shipper_name = empty($row[20]) ? null : $row[20];
                    $order->payment_date = empty($row[21]) ? null : $row[21];
                    $order->shipped_date = empty($row[22]) ? null : $row[22];
                    $order->tracking_number = empty($row[23]) ? null : $row[23];
                    $order->tax_total_cents = empty($row[24]) ? null : $row[24];
                    $order->created_at = Carbon::createFromFormat('Y-m-d H:i:s', $row[25]);
                    $order->updated_at = Carbon::createFromFormat('Y-m-d H:i:s', $row[26]);
                    $order->save();
                    $update++;
                }

            }
            fclose($handle);

            echo '-- -- Successfully inserted ' . $new . ' orders and updated ' . $update . ' orders.' . PHP_EOL;
            Log::info('Successfully inserted ' . $new . ' orders and updated ' . $update . ' orders.');

        } catch (\Exception $e) {

            echo $e->getMessage() . PHP_EOL;
            Log::critical($e);

        }

        $ended = Carbon::now();
        echo "-- " . $this->signature . " Ended at: ". $ended->format('c') . " - Duration : " . $started->diffInSeconds($ended) . " seconds" . PHP_EOL;
        Log::info($this->signature . " Ended at: ". $ended->format('c') . " - Duration : " . $started->diffInSeconds($ended) . " seconds");
    }
}
