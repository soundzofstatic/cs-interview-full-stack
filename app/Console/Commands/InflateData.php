<?php

namespace App\Console\Commands;

use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

class InflateData extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'inflate:data';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command used to insert data into system from CSV files';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $started = Carbon::now();
        echo "-- " . $this->signature . " Started at: ". $started->format('c') . PHP_EOL;
        Log::info($this->signature . " Started at: ". $started->format('c'));

        try {

            $this->call('inflate:data:users');
            $this->call('inflate:data:products');
            $this->call('inflate:data:inventory');
            $this->call('inflate:data:orders');

        } catch (\Exception $e) {

            echo $e->getMessage() . PHP_EOL;
            Log::critical($e);

        }

        $ended = Carbon::now();
        echo "-- " . $this->signature . " Ended at: ". $ended->format('c') . " - Duration : " . $started->diffInSeconds($ended) . " seconds" . PHP_EOL;
        Log::info($this->signature . " Ended at: ". $ended->format('c') . " - Duration : " . $started->diffInSeconds($ended) . " seconds");
    }
}
