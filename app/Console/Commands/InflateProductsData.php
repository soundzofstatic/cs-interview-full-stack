<?php

namespace App\Console\Commands;

use App\Models\Product;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;

class InflateProductsData extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'inflate:data:products';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command used to insert Product data into system from CSV file';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $started = Carbon::now();
        echo "-- " . $this->signature . " Started at: ". $started->format('c') . PHP_EOL;
        Log::info($this->signature . " Started at: ". $started->format('c'));

        try {

            $relativeFilePath = 'data/products.csv';

            if(!Storage::disk('local')->exists($relativeFilePath)) {

                throw new \Exception('Products data file cannot be found');

            }

            $absoluteFilePath = Storage::path($relativeFilePath);
            $handle = fopen($absoluteFilePath, 'r');
            $rows = 0;
            $new = 0;
            $update = 0;

            while (($row = fgetcsv($handle, 1000, ",")) !== FALSE) {

                $rows++;

                if($rows % 100 == 0) {

                    echo '-- -- On row  ' . $rows . PHP_EOL;

                }

                if($rows == 1) { // Skip Header Row
                    continue;
                }

                $product = Product::find($row[0]);

                if(empty($product)) {

                    $product = new Product();
                    $product->id = $row[0];
                    $product->name = $row[1];
                    $product->description = $row[2];
                    $product->style = $row[3];
                    $product->brand = $row[4];
                    $product->url = empty($row[7]) ? null : $row[7];
                    $product->type = $row[8];
                    $product->shipping_price = $row[9];
                    $product->note = empty($row[10]) ? null : $row[10];
                    $product->admin_id = $row[11];
                    $product->created_at = Carbon::createFromFormat('Y-m-d H:i:s', $row[5]);
                    $product->updated_at = Carbon::createFromFormat('Y-m-d H:i:s', $row[6]);
                    $product->save();
                    $new++;

                } else {

                    // Product ID already exists
                    $product = new Product();
                    $product->name = $row[1];
                    $product->description = $row[2];
                    $product->style = $row[3];
                    $product->brand = $row[4];
                    $product->url = empty($row[7]) ? null : $row[7];
                    $product->type = $row[8];
                    $product->shipping_price = $row[9];
                    $product->note = empty($row[10]) ? null : $row[10];
                    $product->admin_id = $row[11];
                    $product->created_at = Carbon::createFromFormat('Y-m-d H:i:s', $row[5]);
                    $product->updated_at = Carbon::createFromFormat('Y-m-d H:i:s', $row[6]);
                    $product->save();
                    $update++;
                }

            }
            fclose($handle);

            echo '-- -- Successfully inserted ' . $new . ' products and updated ' . $update . ' products.' . PHP_EOL;
            Log::info('Successfully inserted ' . $new . ' products and updated ' . $update . ' products.');

        } catch (\Exception $e) {

            echo $e->getMessage() . PHP_EOL;
            Log::critical($e);

        }

        $ended = Carbon::now();
        echo "-- " . $this->signature . " Ended at: ". $ended->format('c') . " - Duration : " . $started->diffInSeconds($ended) . " seconds" . PHP_EOL;
        Log::info($this->signature . " Ended at: ". $ended->format('c') . " - Duration : " . $started->diffInSeconds($ended) . " seconds");
    }
}
