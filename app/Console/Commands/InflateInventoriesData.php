<?php

namespace App\Console\Commands;

use App\Models\Inventory;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;

class InflateInventoriesData extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'inflate:data:inventory';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command used to insert Inventory data into system from CSV file';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $started = Carbon::now();
        echo "-- " . $this->signature . " Started at: ". $started->format('c') . PHP_EOL;
        Log::info($this->signature . " Started at: ". $started->format('c'));

        try {

            $relativeFilePath = 'data/inventory.csv';

            if(!Storage::disk('local')->exists($relativeFilePath)) {

                throw new \Exception('Inventory data file cannot be found');

            }

            $absoluteFilePath = Storage::path($relativeFilePath);
            $handle = fopen($absoluteFilePath, 'r');
            $rows = 0;
            $new = 0;
            $update = 0;

            while (($row = fgetcsv($handle, 1000, ",")) !== FALSE) {

                $rows++;

                if($rows % 100 == 0) {

                    echo '-- -- On row  ' . $rows . PHP_EOL;

                }

                if($rows == 1) { // Skip Header Row
                    continue;
                }

                $inventory = Inventory::find($row[0]);

                if(empty($inventory)) {

                    $inventory = new Inventory();
                    $inventory->id = $row[0];
                    $inventory->product_id = $row[1];
                    $inventory->quantity = $row[2];
                    $inventory->color = $row[3];
                    $inventory->size = $row[4];
                    $inventory->weight = $row[5];
                    $inventory->price_cents = $row[6];
                    $inventory->sale_price_cents = $row[7];
                    $inventory->cost_cents = $row[8];
                    $inventory->sku = $row[9];
                    $inventory->length = $row[10];
                    $inventory->width = $row[11];
                    $inventory->height = $row[12];
                    $inventory->note = empty($row[13]) ? null : $row[13];
                    $inventory->save();
                    $new++;

                } else {

                    // Inventory ID already exists
                    $inventory = new Inventory();
                    $inventory->product_id = $row[1];
                    $inventory->quantity = $row[2];
                    $inventory->color = $row[3];
                    $inventory->size = $row[4];
                    $inventory->weight = $row[5];
                    $inventory->price_cents = $row[6];
                    $inventory->sale_price_cents = $row[7];
                    $inventory->cost_cents = $row[8];
                    $inventory->sku = $row[9];
                    $inventory->length = $row[10];
                    $inventory->width = $row[11];
                    $inventory->height = $row[12];
                    $inventory->note = empty($row[13]) ? null : $row[13];
                    $inventory->save();
                    $update++;
                }

            }
            fclose($handle);

            echo '-- -- Successfully inserted ' . $new . ' inventory and updated ' . $update . ' inventory.' . PHP_EOL;
            Log::info('Successfully inserted ' . $new . ' inventory and updated ' . $update . ' inventory.');

        } catch (\Exception $e) {

            echo $e->getMessage() . PHP_EOL;
            Log::critical($e);

        }

        $ended = Carbon::now();
        echo "-- " . $this->signature . " Ended at: ". $ended->format('c') . " - Duration : " . $started->diffInSeconds($ended) . " seconds" . PHP_EOL;
        Log::info($this->signature . " Ended at: ". $ended->format('c') . " - Duration : " . $started->diffInSeconds($ended) . " seconds");

    }

}
