<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;

    public function skus()
    {

        return $this->hasMany(
            Inventory::class,
            'product_id',
            'id'
        );

    }

    // todo - Should be a global utility
    public static function convertToUsDollars($cents = 0)
    {

        $fmt = numfmt_create( 'en_US', \NumberFormatter::CURRENCY);
        return numfmt_format_currency($fmt, ($cents / 100), "USD");

    }
}
