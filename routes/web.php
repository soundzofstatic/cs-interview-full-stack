<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome-bootstrap');
});
Route::get('/welcome-default', function () {
    return view('welcome');
});

Route::prefix('dashboard')->name('dashboard.')->middleware(['auth'])->group(function () {
    Route::get('/', [\App\Http\Controllers\DashboardController::class, 'index'])->name('home');

    Route::prefix('product')->name('product.')->group(function () {
        Route::get('/', [\App\Http\Controllers\ProductController::class, 'index'])->name('home');
        Route::get('/create', [\App\Http\Controllers\ProductController::class, 'create'])->name('create');
        Route::post('/store', [\App\Http\Controllers\ProductController::class, 'store'])->name('store');
        Route::prefix('{product}')->group(function () {
            Route::get('/', [\App\Http\Controllers\ProductController::class, 'show'])->name('show');
            Route::get('/edit', [\App\Http\Controllers\ProductController::class, 'edit'])->name('edit');
            Route::put('/edit', [\App\Http\Controllers\ProductController::class, 'update'])->name('update');
            Route::delete('/delete', [\App\Http\Controllers\ProductController::class, 'destroy'])->name('delete');
            Route::get('/create-inventory', [\App\Http\Controllers\ProductController::class, 'createInventory'])->name('create-inventory');
            Route::post('/store-inventory', [\App\Http\Controllers\ProductController::class, 'storeInventory'])->name('store-inventory');
        });
    });

    Route::prefix('inventory')->name('inventory.')->group(function () {
        Route::get('/{reset?}', [\App\Http\Controllers\InventoryController::class, 'index'])->name('home');
        Route::post('/', [\App\Http\Controllers\InventoryController::class, 'search'])->name('search');
        Route::post('/reset', [\App\Http\Controllers\InventoryController::class, 'searchReset'])->name('search-reset');
        Route::prefix('{inventory}')->group(function () {
            Route::get('/edit', [\App\Http\Controllers\InventoryController::class, 'edit'])->name('edit');
            Route::put('/edit', [\App\Http\Controllers\InventoryController::class, 'update'])->name('update');
            Route::delete('/delete', [\App\Http\Controllers\InventoryController::class, 'destroy'])->name('delete');
        });
    });

});

require __DIR__.'/auth.php';
