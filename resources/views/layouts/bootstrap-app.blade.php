<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors">
    <meta name="generator" content="Hugo 0.83.1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>{{ config('app.name', 'Laravel') }}</title>

    <link rel="canonical" href="https://getbootstrap.com/docs/5.0/examples/dashboard/">
    <!-- Fonts -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap">
    <!-- Styles -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">
    <!-- Scripts -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-gtEjrD/SeCtmISkJkNUaaKMoLD0//ElJ19smozuHV6z3Iehds+3Ulb9Bn9Plx0x4" crossorigin="anonymous"></script>

    <!-- Bootstrap core CSS -->
    <style>
        .bd-placeholder-img {
            font-size: 1.125rem;
            text-anchor: middle;
            -webkit-user-select: none;
            -moz-user-select: none;
            user-select: none;
        }

        @media (min-width: 768px) {
            .bd-placeholder-img-lg {
                font-size: 3.5rem;
            }
        }
    </style>


    <!-- Custom styles for this template -->
    <link href="{{ asset('css/dashboard.css') }}" rel="stylesheet">
</head>
<body>

<header class="navbar navbar-dark sticky-top bg-dark flex-md-nowrap p-0 shadow">
    <a class="navbar-brand col-md-3 col-lg-2 me-0 px-3" href="#">{{ env('APP_NAME') }}</a>
    <button class="navbar-toggler position-absolute d-md-none collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#sidebarMenu" aria-controls="sidebarMenu" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    {{--    <input class="form-control form-control-dark w-100" type="text" placeholder="Search" aria-label="Search">--}}
    @guest()
        <ul class="navbar-nav px-3">
            <li class="nav-item text-nowrap">
                <a class="nav-link" href="{{ route('login') }}">Log in</a>
            </li>
        </ul>
    @endguest
    @auth()
        <ul class="navbar-nav px-3">
            <li class="nav-item text-nowrap">
                <form method="POST" action="{{ route('logout') }}">
                    @csrf
                    <button type="submit" class="btn btn-sm btn-danger">Sign Out</button>
                </form>
            </li>
        </ul>
    @endauth
</header>

<div class="container-fluid">
    <div class="row">
        @auth
            <nav id="sidebarMenu" class="col-md-3 col-lg-2 d-md-block bg-light sidebar collapse">
                <div class="position-sticky pt-3">
                    <ul class="nav flex-column">
                        <li class="nav-item">
                            <a class="nav-link active" aria-current="page" href="{{ route('dashboard.home') }}">
                                <span data-feather="home"></span>
                                Dashboard
                            </a>
                        </li>
{{--                        <li class="nav-item">--}}
{{--                            <a class="nav-link" href="#">--}}
{{--                                <span data-feather="file"></span>--}}
{{--                                Orders--}}
{{--                            </a>--}}
{{--                        </li>--}}
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('dashboard.product.home') }}">
                                <span data-feather="shopping-cart"></span>
                                Products
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('dashboard.inventory.home') }}">
                                <span data-feather="users"></span>
                                Inventory
                            </a>
                        </li>
{{--                        <li class="nav-item">--}}
{{--                            <a class="nav-link" href="#">--}}
{{--                                <span data-feather="bar-chart-2"></span>--}}
{{--                                Reports--}}
{{--                            </a>--}}
{{--                        </li>--}}
{{--                        <li class="nav-item">--}}
{{--                            <a class="nav-link" href="#">--}}
{{--                                <span data-feather="layers"></span>--}}
{{--                                Integrations--}}
{{--                            </a>--}}
{{--                        </li>--}}
                    </ul>

{{--                    <h6 class="sidebar-heading d-flex justify-content-between align-items-center px-3 mt-4 mb-1 text-muted">--}}
{{--                        <span>Saved reports</span>--}}
{{--                        <a class="link-secondary" href="#" aria-label="Add a new report">--}}
{{--                            <span data-feather="plus-circle"></span>--}}
{{--                        </a>--}}
{{--                    </h6>--}}
{{--                    <ul class="nav flex-column mb-2">--}}
{{--                        <li class="nav-item">--}}
{{--                            <a class="nav-link" href="#">--}}
{{--                                <span data-feather="file-text"></span>--}}
{{--                                Current month--}}
{{--                            </a>--}}
{{--                        </li>--}}
{{--                        <li class="nav-item">--}}
{{--                            <a class="nav-link" href="#">--}}
{{--                                <span data-feather="file-text"></span>--}}
{{--                                Last quarter--}}
{{--                            </a>--}}
{{--                        </li>--}}
{{--                        <li class="nav-item">--}}
{{--                            <a class="nav-link" href="#">--}}
{{--                                <span data-feather="file-text"></span>--}}
{{--                                Social engagement--}}
{{--                            </a>--}}
{{--                        </li>--}}
{{--                        <li class="nav-item">--}}
{{--                            <a class="nav-link" href="#">--}}
{{--                                <span data-feather="file-text"></span>--}}
{{--                                Year-end sale--}}
{{--                            </a>--}}
{{--                        </li>--}}
{{--                    </ul>--}}
                </div>
            </nav>
        @endauth

        <main class="col-md-9 ms-sm-auto col-lg-10 px-md-4">
            <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
                <h1 class="h2">{{ $header }}</h1>
                {{--                <div class="btn-toolbar mb-2 mb-md-0">--}}
                {{--                    <div class="btn-group me-2">--}}
                {{--                        <button type="button" class="btn btn-sm btn-outline-secondary">Share</button>--}}
                {{--                        <button type="button" class="btn btn-sm btn-outline-secondary">Export</button>--}}
                {{--                    </div>--}}
                {{--                    <button type="button" class="btn btn-sm btn-outline-secondary dropdown-toggle">--}}
                {{--                        <span data-feather="calendar"></span>--}}
                {{--                        This week--}}
                {{--                    </button>--}}
                {{--                </div>--}}
            </div>
            @auth
                <section id="breadcrumbs">
                    <div id="breadcrumb-wrapper" class="row">
                        <div class="col">
                            <nav id="breadcrumbs" style="--bs-breadcrumb-divider: url(&#34;data:image/svg+xml,%3Csvg xmlns='http://www.w3.org/2000/svg' width='8' height='8'%3E%3Cpath d='M2.5 0L1 1.5 3.5 4 1 6.5 2.5 8l4-4-4-4z' fill='currentColor'/%3E%3C/svg%3E&#34;);" aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="{{ route('dashboard.home') }}">Dashboard</a></li>
                                    {{ !empty($breadcrumb) ? $breadcrumb : '' }}
                                </ol>
                            </nav>
                        </div>
                    </div>
                </section>
            @endauth

            @if ($message = Session::get('success'))
                <section id="success-alert">
                    <div class="row">
                        <div class="alert alert-success alert-block">
                            <span class="ri-close-circle-fill" data-dismiss="alert"></span>
                            <strong>{{ $message }}</strong>
                        </div>
                    </div>
                </section>
            @endif

            @if ($message = Session::get('error'))
                <section id="error-alert">
                    <div class="row">
                        <div class="alert alert-danger alert-block">
                            <span class="ri-close-circle-fill" data-dismiss="alert"></span>
                            <strong>{{ $message }}</strong>
                        </div>
                    </div>
                </section>
            @endif

            @if ($message = Session::get('warning'))
                <section id="warning-alert">
                    <div class="row">
                        <div class="alert alert-warning alert-block">
                            <span class="ri-close-circle-fill" data-dismiss="alert"></span>
                            <strong>{{ $message }}</strong>
                        </div>
                    </div>
                </section>
            @endif

            @if ($message = Session::get('info'))
                <section id="info-alert">
                    <div class="row">
                        <div class="alert alert-info alert-block">
                            <span class="ri-close-circle-fill" data-dismiss="alert"></span>
                            <strong>{{ $message }}</strong>
                        </div>
                    </div>
                </section>
            @endif

            @if ($errors->any())
                <section id="error-alert">
                    <div class="row">
                        <div class="alert alert-danger">
                            <span class="ri-close-circle-fill" data-dismiss="alert"></span>
                            Check the following errors :(
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                </section>
            @endif
            {{ !empty($content) ? $content : '' }}
        </main>
    </div>
</div>


{{--    <script src="../assets/dist/js/bootstrap.bundle.min.js"></script>--}}
<script src="https://cdn.jsdelivr.net/npm/feather-icons@4.28.0/dist/feather.min.js" integrity="sha384-uO3SXW5IuS1ZpFPKugNNWqTZRRglnUJK6UAZ/gxOX80nxEkN9NcGZTftn6RzhGWE" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/chart.js@2.9.4/dist/Chart.min.js" integrity="sha384-zNy6FEbO50N+Cg5wap8IKA4M/ZnLJgzc6w2NqACZaK0u0FXfOWRRJOnQtpZun8ha" crossorigin="anonymous"></script>
<script src="{{ asset('js/dashboard.js') }}"></script>
{{ !empty($scripts) ? $scripts : '' }}
</body>
</html>
