<x-bootstrap-app-layout>
    <x-slot name="header">{{ __('Home') }}</x-slot>
    <x-slot name="content">
        <section>
            <div class="row justify-content-md-center">
                <div class="col-md-6">
                    <div class="card text-center">
                        <div class="card-header">
                            <h2>Login to see the Administrative Dashboard</h2>
                        </div>
                        <div class="card-body">
                            <form method="POST" action="{{ route('login') }}" class="row g-3">
                                @csrf
                                <div class="col-md-12">
                                    <label for="email" class="form-label">Email</label>
                                    <input type="email" class="form-control @error('email') is-invalid @enderror" id="email" name="email" value="{{ old('email') }}"  />
                                    @error('email')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                                <div class="col-md-12">
                                    <label for="password" class="form-label">Password</label>
                                    <input type="password" class="form-control @error('password') is-invalid @enderror" id="password" name="password" />
                                    @error('password')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                                <div class="col-12">
                                    <button type="submit" class="btn btn-primary">Sign in</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </x-slot>
</x-bootstrap-app-layout>
