<x-bootstrap-app-layout>
    <x-slot name="breadcrumb">
        <li class="breadcrumb-item active" aria-current="page">{{ __('Products') }}</li>
    </x-slot>
    <x-slot name="header">Product Edit</x-slot>
    <x-slot name="content">
        <section>
            <div class="row">
                <div class="col-md-12">
                    <h2>Edit a Product</h2>
                    <p>Fill out the form below to create a new product.</p>
                    <div class="row">
                        <div class="col">
                            <form class="row g-3" method="POST" action="{{ route('dashboard.product.update', ['product' => $product->id]) }}">
                                @csrf
                                @method('PUT')
                                <div class="col-md-6">
                                    <label for="name" class="form-label">Product Name</label>
                                    <input type="text" class="form-control @error('name') is-invalid @enderror" id="name" name="name" value="{{ old('name', $product->name) }}" required />
                                    @error('name')
                                        <div class="invalid-feedback">
                                            {{ $message }}
                                        </div>
                                    @enderror
                                </div>
                                <div class="col-md-6">
                                    <label for="brand" class="form-label">Product Brand</label>
                                    <input type="text" class="form-control @error('brand') is-invalid @enderror" id="brand" name="brand" value="{{ old('brand', $product->brand) }}" required />
                                    @error('brand')
                                    <div class="invalid-feedback">
                                        {{ $message }}
                                    </div>
                                    @enderror
                                </div>
                                <div class="col-md-6">
                                    <label for="style" class="form-label">Product Style</label>
                                    <input type="text" class="form-control @error('style') is-invalid @enderror" id="name" name="style" value="{{ old('style', $product->style) }}" required />
                                    @error('style')
                                    <div class="invalid-feedback">
                                        {{ $message }}
                                    </div>
                                    @enderror
                                </div>
                                <div class="col-md-4">
                                    <label for="type" class="form-label">Product Type</label>
                                    <select id="type" class="form-select @error('type') is-invalid @enderror" name="type" required>
                                        <option value="" selected>Choose...</option>
                                        <option value="clothing" {{ old('type', $product->type) == 'clothing' ? 'selected="selected"' : '' }}>Clothing</option>
                                    </select>
                                    @error('type')
                                    <div class="invalid-feedback">
                                        {{ $message }}
                                    </div>
                                    @enderror
                                </div>
                                <div class="col-md-2">
                                    <label for="shipping_price" class="form-label">Shipping Price (USD)</label>
                                    <input type="text" class="form-control @error('shipping_price') is-invalid @enderror" id="shipping_price" name="shipping_price" value="{{ old('shipping_price', \App\Models\Product::convertToUsDollars($product->shipping_price)) }}" placeholder="0.00" required />
                                    @error('shipping_price')
                                    <div class="invalid-feedback">
                                        {{ $message }}
                                    </div>
                                    @enderror
                                </div>
                                <div class="col-md-6">
                                    <label for="description" class="form-label">Product Description</label>
                                    <textarea name="description" id="description" class="form-control  @error('description') is-invalid @enderror" required>{{ old('description', $product->description) }}</textarea>
                                    @error('description')
                                    <div class="invalid-feedback">
                                        {{ $message }}
                                    </div>
                                    @enderror
                                </div>
                                <div class="col-md-6">
                                    <label for="note" class="form-label">Note</label>
                                    <textarea name="note" id="note" class="form-control  @error('note') is-invalid @enderror">{{ old('note', $product->note) }}</textarea>
                                    @error('note')
                                    <div class="invalid-feedback">
                                        {{ $message }}
                                    </div>
                                    @enderror
                                </div>
                                <div class="col-md-3">
                                    <label for="url" class="form-label">Product URL</label>
                                    <input type="url" class="form-control @error('url') is-invalid @enderror" id="url" name="url" value="{{ old('url', $product->url) }}" />
                                    @error('url')
                                    <div class="invalid-feedback">
                                        {{ $message }}
                                    </div>
                                    @enderror
                                </div>
                                <div class="col-12">
                                    <button type="submit" class="btn btn-primary">Update</button>
                                    <a href="{{ route('dashboard.product.show', ['product' => $product->id]) }}" class="btn btn-danger">Cancel</a>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </x-slot>
</x-bootstrap-app-layout>
