<x-bootstrap-app-layout>
    <x-slot name="breadcrumb">
        <li class="breadcrumb-item active" aria-current="page">{{ __('Inventory') }}</li>
    </x-slot>
    <x-slot name="header">Inventory Edit</x-slot>
    <x-slot name="content">
        <section>
            <div class="row">
                <div class="col-md-12">
                    <h2>Edit Inventory</h2>
                    <p>Fill out the form below to edit this inventory.</p>
                    <div class="row">
                        <div class="col">
                            <form class="row g-3" method="POST" action="{{ route('dashboard.product.store-inventory', ['product' => $product->id]) }}">
                                @csrf
                                <div class="col-md-6">
                                    <label for="name" class="form-label">Product Name</label>
                                    <input type="text" class="form-control @error('name') is-invalid @enderror" id="name" name="name" value="{{ old('name', $product->name) }}" disabled />
                                    @error('name')
                                        <div class="invalid-feedback">
                                            {{ $message }}
                                        </div>
                                    @enderror
                                </div>
                                <div class="col-md-6">
                                    <label for="brand" class="form-label">Product Brand</label>
                                    <input type="text" class="form-control @error('brand') is-invalid @enderror" id="brand" name="brand" value="{{ old('brand', $product->brand) }}" disabled />
                                    @error('brand')
                                    <div class="invalid-feedback">
                                        {{ $message }}
                                    </div>
                                    @enderror
                                </div>
                                <div class="col-md-6">
                                    <label for="style" class="form-label">Product Style</label>
                                    <input type="text" class="form-control @error('style') is-invalid @enderror" id="name" name="style" value="{{ old('style', $product->style) }}" disabled />
                                    @error('style')
                                    <div class="invalid-feedback">
                                        {{ $message }}
                                    </div>
                                    @enderror
                                </div>
                                <div class="col-md-6">
                                    <label for="type" class="form-label">Product Type</label>
                                    <input type="text" class="form-control @error('type') is-invalid @enderror" id="name" name="type" value="{{ old('type', $product->type) }}" disabled />
                                    @error('type')
                                    <div class="invalid-feedback">
                                        {{ $message }}
                                    </div>
                                    @enderror
                                </div>

                                <div class="col-md-4">
                                    <label for="sku" class="form-label">SKU</label>
                                    <input type="text" class="form-control @error('sku') is-invalid @enderror" id="sku" name="sku" value="{{ old('sku') }}" placeholder="0" required />
                                    @error('sku')
                                    <div class="invalid-feedback">
                                        {{ $message }}
                                    </div>
                                    @enderror
                                </div>

                                <div class="col-md-2">
                                    <label for="shipping_price" class="form-label">Product Shipping Price (USD)</label>
                                    <input type="text" class="form-control @error('shipping_price') is-invalid @enderror" id="shipping_price" name="shipping_price" value="{{ old('shipping_price', \App\Models\Product::convertToUsDollars($product->shipping_price)) }}" placeholder="0.00" disabled />
                                    @error('shipping_price')
                                    <div class="invalid-feedback">
                                        {{ $message }}
                                    </div>
                                    @enderror
                                </div>

                                <div class="col-md-2">
                                    <label for="price" class="form-label">Price (USD)</label>
                                    <input type="text" class="form-control @error('price') is-invalid @enderror" id="price" name="price" value="{{ old('price') }}" placeholder="0.00" required />
                                    @error('price')
                                    <div class="invalid-feedback">
                                        {{ $message }}
                                    </div>
                                    @enderror
                                </div>

                                <div class="col-md-2">
                                    <label for="sale_price" class="form-label">Sale Price (USD)</label>
                                    <input type="text" class="form-control @error('sale_price') is-invalid @enderror" id="sale_price" name="sale_price" value="{{ old('sale_price') }}" placeholder="0.00" required />
                                    @error('sale_price')
                                    <div class="invalid-feedback">
                                        {{ $message }}
                                    </div>
                                    @enderror
                                </div>

                                <div class="col-md-2">
                                    <label for="cost" class="form-label">Cost (USD)</label>
                                    <input type="text" class="form-control @error('cost') is-invalid @enderror" id="cost" name="cost" value="{{ old('cost') }}" placeholder="0.00" required />
                                    @error('cost')
                                    <div class="invalid-feedback">
                                        {{ $message }}
                                    </div>
                                    @enderror
                                </div>

                                <div class="col-md-1">
                                    <label for="quantity" class="form-label">Quantity</label>
                                    <input type="text" class="form-control @error('quantity') is-invalid @enderror" id="quantity" name="quantity" value="{{ old('quantity') }}" placeholder="0" required />
                                    @error('quantity')
                                    <div class="invalid-feedback">
                                        {{ $message }}
                                    </div>
                                    @enderror
                                </div>
                                <div class="col-md-6">
                                    <label for="color" class="form-label">Color</label>
                                    <input type="text" class="form-control @error('color') is-invalid @enderror" id="color" name="color" value="{{ old('color') }}" placeholder="0" required />
                                    @error('color')
                                    <div class="invalid-feedback">
                                        {{ $message }}
                                    </div>
                                    @enderror
                                </div>
                                <div class="col-md-1">
                                    <label for="size" class="form-label">Size</label>
                                    <input type="text" class="form-control @error('size') is-invalid @enderror" id="size" name="size" value="{{ old('size') }}" placeholder="0" required />
                                    @error('size')
                                    <div class="invalid-feedback">
                                        {{ $message }}
                                    </div>
                                    @enderror
                                </div>

                                <div class="col-md-1">
                                    <label for="weight" class="form-label">Weight</label>
                                    <input type="text" class="form-control @error('weight') is-invalid @enderror" id="weight" name="weight" value="{{ old('weight') }}" placeholder="0" required />
                                    @error('weight')
                                    <div class="invalid-feedback">
                                        {{ $message }}
                                    </div>
                                    @enderror
                                </div>
                                <div class="col-md-1">
                                    <label for="length" class="form-label">Length</label>
                                    <input type="text" class="form-control @error('length') is-invalid @enderror" id="length" name="length" value="{{ old('length') }}" placeholder="0" required />
                                    @error('length')
                                    <div class="invalid-feedback">
                                        {{ $message }}
                                    </div>
                                    @enderror
                                </div>
                                <div class="col-md-1">
                                    <label for="width" class="form-label">Width</label>
                                    <input type="text" class="form-control @error('width') is-invalid @enderror" id="width" name="width" value="{{ old('width') }}" placeholder="0" required />
                                    @error('width')
                                    <div class="invalid-feedback">
                                        {{ $message }}
                                    </div>
                                    @enderror
                                </div>
                                <div class="col-md-1">
                                    <label for="height" class="form-label">Height</label>
                                    <input type="text" class="form-control @error('height') is-invalid @enderror" id="height" name="height" value="{{ old('height') }}" placeholder="0" required />
                                    @error('height')
                                    <div class="invalid-feedback">
                                        {{ $message }}
                                    </div>
                                    @enderror
                                </div>
                                <div class="col-md-6">
                                    <label for="note" class="form-label">Note</label>
                                    <textarea name="note" id="note" class="form-control  @error('note') is-invalid @enderror">{{ old('note') }}</textarea>
                                    @error('note')
                                    <div class="invalid-feedback">
                                        {{ $message }}
                                    </div>
                                    @enderror
                                </div>
                                <div class="col-12">
                                    <button type="submit" class="btn btn-primary">Submit</button>
                                    <a href="{{ route('dashboard.inventory.home') }}" class="btn btn-danger">Cancel</a>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </x-slot>
</x-bootstrap-app-layout>
