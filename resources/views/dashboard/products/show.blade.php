<x-bootstrap-app-layout>
    <x-slot name="breadcrumb">
        <li class="breadcrumb-item"><a href="{{ route('dashboard.product.home') }}">{{ __('Products') }}</a></li>
        <li class="breadcrumb-item active" aria-current="page">{{ __('Product: ') }} {{ $product->name }}</li>
    </x-slot>
    <x-slot name="header">Product</x-slot>
    <x-slot name="content">
        <section class="mb-5">
            <div class="row">
                <div class="col-md-12">
                    <h2>Product: {{ $product->name }}</h2>
                    <div class="row">
                        <div class="col-md-12">
                            <table class="table table-hover">
                                <thead>
                                <th>Product ID</th>
                                <th>Name</th>
                                <th>Brand</th>
                                <th>Shipping Price</th>
                                <th>Style</th>
                                <th>URL</th>
                                <th>Actions</th>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>{{ $product->id }}</td>
                                        <td>{{ $product->name }}</td>
                                        <td>{{ $product->brand }}</td>
                                        <td>{{ \App\Models\Product::convertToUsDollars($product->shipping_price) }}</td>
                                        <td>{{ $product->style }}</td>
                                        <td>{{ $product->url }}</td>
                                        <td>
                                            <div class="row m-1">
                                                <div class="col">
                                                    <a href="{{ route('dashboard.product.edit', ['product' => $product->id]) }}" class="btn btn-sm btn-warning">Edit</a>
                                                </div>
                                            </div>
                                            <div class="row m-1">
                                                <div class="col">
                                                    <form class="row" method="POST" action="{{ route('dashboard.product.delete', ['product' => $product->id]) }}" class="">
                                                        @csrf
                                                        {{method_field('DELETE')}}
                                                        <div class="col-2">
                                                            <input type="hidden" name="product_id" value="{{ $product->id }}" />
                                                            <button type="submit" class="btn btn-sm btn-danger">Delete</button>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="col-md-12">
                    <h3>Description</h3>
                    <p>{{ $product->description }}</p>
                </div>
                @if(!empty($product->note))
                    <div class="col-md-12">
                        <h3>Note</h3>
                        <p>{{ $product->note }}</p>
                    </div>
                @endif
            </div>
        </section>
        <section>
            <div class="row">
                <div class="col-md-12">
                    <h2>Product Inventory</h2>
                    <div class="row">
                        <div class="col">
                            <a href="{{ route('dashboard.product.create-inventory', ['product' => $product->id]) }}" class="btn btn btn-success">Create New Inventory</a>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <table class="table table-hover">
                                <thead>
                                <th>SKU</th>
                                <th>Quantity</th>
                                <th>Color</th>
                                <th>Size</th>
                                <th>Weight</th>
                                <th>Price</th>
                                <th>Sale Price</th>
                                <th>Cost</th>
                                <th>Length</th>
                                <th>Width</th>
                                <th>Height</th>
{{--                                <th>Actions</th>--}}
                                </thead>
                                <tbody>
                                @foreach($product->skus as $inventory)
                                    <tr>
                                        <td>{{ $inventory->sku }}</td>
                                        <td>{{ $inventory->quantity }}</td>
                                        <td>{{ $inventory->color }}</td>
                                        <td>{{ $inventory->size }}</td>
                                        <td>{{ $inventory->weight }}</td>
                                        <td>{{ \App\Models\Product::convertToUsDollars($inventory->price_cents) }}</td>
                                        <td>{{ \App\Models\Product::convertToUsDollars($inventory->sale_price_cents) }}</td>
                                        <td>{{ \App\Models\Product::convertToUsDollars($inventory->cost_cents) }}</td>
                                        <td>{{ $inventory->length }}</td>
                                        <td>{{ $inventory->width }}</td>
                                        <td>{{ $inventory->height }}</td>
{{--                                        <td></td>--}}
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </x-slot>
</x-bootstrap-app-layout>
