<x-bootstrap-app-layout>
    <x-slot name="breadcrumb">
        <li class="breadcrumb-item active" aria-current="page">{{ __('Products') }} Page {{ $products->currentPage() }}/{{ $products->lastPage() }}</li>
    </x-slot>
    <x-slot name="header">Products</x-slot>
    <x-slot name="content">
        <section>
            <div class="row mt-5">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col">
                            <a href="{{ route('dashboard.product.create') }}" class="btn btn btn-success">Create New Product</a>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <table class="table table-hover">
                                <thead>
                                <th>Product ID</th>
                                <th>Name</th>
                                <th>Brand</th>
                                <th>Style</th>
                                <th>SKUs</th>
                                <th>Actions</th>
                                </thead>
                                <tbody>
                                @foreach($products as $product)
                                    <tr>
                                        <td>{{ $product->id }}</td>
                                        <td>{{ $product->name }}</td>
                                        <td>{{ $product->brand }}</td>
                                        <td>{{ $product->style }}</td>
                                        <td>
                                            <ul>
                                                @foreach($product->skus as $sku)
                                                    <li>{{ $sku->sku }}</li>
                                                @endforeach
                                            </ul>
                                        </td>
                                        <td>
                                            <div class="row m-1">
                                                <div class="col">
                                                    <a href="{{ route('dashboard.product.show', ['product' => $product->id]) }}" class="btn btn-sm btn-warning">View</a>
                                                </div>
                                            </div>
                                            <div class="row m-1">
                                                <div class="col">
                                                    <form class="row" method="POST" action="{{ route('dashboard.product.delete', ['product' => $product->id]) }}" class="">
                                                        @csrf
                                                        {{method_field('DELETE')}}
                                                        <div class="col-2">
                                                            <input type="hidden" name="product_id" value="{{ $product->id }}" />
                                                            <button type="submit" class="btn btn-sm btn-danger">Delete</button>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            <nav class="pagination" aria-label="Page navigation example">
                                <ul class="pagination">
                                    <li class="page-item {{ ($products->currentPage() == 1) ? ' disabled' : '' }}">
                                        @if($products->currentPage() == 1)
                                            <a href="#" onclick="return false;" class="page-link">Previous</a>
                                        @else
                                            <a href="{{ $products->url(1) }}" class="page-link">Previous</a>
                                        @endif
                                    </li>
                                    @for ($i = 1; $i <= $products->lastPage(); $i++)
                                        <li class="page-item {{ ($products->currentPage() == $i) ? ' active' : '' }}">
                                            <a href="{{ $products->url($i) }}" class="page-link">{{ $i }}</a>
                                        </li>
                                    @endfor
                                    <li class="page-item {{ ($products->currentPage() == $products->lastPage()) ? ' disabled' : '' }}">
                                        @if($products->currentPage() == $products->lastPage())
                                            <a href="#" onclick="return false;" class="page-link">Next</a>
                                        @else
                                            <a href="{{ $products->url($products->currentPage()+1) }}" class="page-link" >Next</a>
                                        @endif
                                    </li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </x-slot>
</x-bootstrap-app-layout>
