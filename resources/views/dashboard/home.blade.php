<x-bootstrap-app-layout>
    <x-slot name="header">Welcome, {{ $user->name }}</x-slot>
    <x-slot name="content">
        <section>
            <div class="row">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-sm-3">
                            <div class="card">
                                <div class="card-body">
                                    <h2 class="card-title">Manage Products</h2>
                                    <a href="{{ route('dashboard.product.home') }}" class="btn btn-primary">Jump to Products</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-3">
                            <div class="card">
                                <div class="card-body">
                                    <h2 class="card-title">Review Inventory</h2>
                                    <a href="{{ route('dashboard.inventory.home') }}" class="btn btn-primary">Jump to Inventory</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </x-slot>
</x-bootstrap-app-layout>
