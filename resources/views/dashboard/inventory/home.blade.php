<x-bootstrap-app-layout>
    <x-slot name="breadcrumb">
        <li class="breadcrumb-item active" aria-current="page">{{ __('Inventory') }} Page {{ $inventory->currentPage() }}/{{ $inventory->lastPage() }}</li>
    </x-slot>
    <x-slot name="header">Inventory</x-slot>
    <x-slot name="content">
        <section>
            <div class="row">
                <div class="col-md-12">
                    <div class="row mb-5 mt-5">
                        <div class="col-md-12">
                            <h3>At a Glance</h3>
                            <div class="row">
                                <div class="col-sm-3">
                                    <div class="card">
                                        <div class="card-header">
                                            Total Unique Inventory Records
                                        </div>
                                        <div class="card-body">
                                            <h4 class="card-title">{{ $allInventory->count() }} Records</h4>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="card">
                                        <div class="card-header">
                                            Total Inventory Quantity
                                        </div>
                                        <div class="card-body">
                                            <h4 class="card-title">{{ $allInventory->sum('quantity') }} pieces</h4>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="card">
                                        <div class="card-header">
                                            Avg. Item Price
                                        </div>
                                        <div class="card-body">
                                            <h4 class="card-title">{{ \App\Models\Product::convertToUsDollars($allInventory->avg('price_cents')) }}</h4>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="card">
                                        <div class="card-header">
                                            Avg. Item Cost
                                        </div>
                                        <div class="card-body">
                                            <h4 class="card-title">{{ \App\Models\Product::convertToUsDollars($allInventory->avg('cost_cents')) }}</h4>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row mb-5">
                        <div class="col">
                            <form class="row g-3 align-items-center" method="POST" action="{{ route('dashboard.inventory.search') }}" class="">
                                @csrf
                                <div class="col-md-6">
                                    <label for="query" class="visually-hidden">Search by Product ID or Inventory SKU</label>
                                    <input type="text" class="form-control @error('query') is-invalid @enderror" id="query" name="query" value="{{ old('query', (!empty(\Illuminate\Support\Facades\Session::get('query')) ? \Illuminate\Support\Facades\Session::get('query') : '')) }}" placeholder="Search by Product ID or Inventory SKU" />
                                    @error('query')
                                    <div class="invalid-feedback">
                                        {{ $message }}
                                    </div>
                                    @enderror
                                </div>
                                <div class="col-md-3">
                                    <label for="quantity_limit" class="form-label">Show Items with quantities less than <strong><span id="qty-slider-value"></span></strong></label>
                                    <input type="range" class="form-range" min="0" max="{{ $allInventory->max('quantity') }}" id="quantity_limit" name="quantity_limit" value="{{ old('quantity_limit', (!empty(\Illuminate\Support\Facades\Session::get('quantity_limit')) ? \Illuminate\Support\Facades\Session::get('quantity_limit') : $allInventory->max('quantity'))) }}">
                                </div>
                                <div class="col-2">
                                    <button type="submit" class="btn btn-primary">Search</button>
                                    <a href="{{ route('dashboard.inventory.home', ['reset' => 'reset']) }}" class="btn btn-danger">Reset</a>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="row mb-2">
                        <div class="col">
                            <h2>Results: {{ $inventory->total() }} Items</h2>
                            @if(!empty(\Illuminate\Support\Facades\Session::get('quantity_limit')))
                                <a href="{{ route('dashboard.inventory.home', ['reset' => 'reset']) }}" class="btn btn-sm btn-danger">Reset Search Results</a>
                            @endif
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <table class="table table-hover">
                                <thead>
                                <th>Product ID</th>
                                <th>Product Name</th>
                                <th>SKU</th>
                                <th>Quantity</th>
                                <th>Color</th>
                                <th>Size</th>
                                <th>Price</th>
                                <th>Cost</th>
                                <th>Actions</th>
                                </thead>
                                <tbody>
                                @foreach($inventory as $item)
                                    <tr>
                                        <td>{{ $item->product->id }}</td>
                                        <td>{{ $item->product->name }}</td>
                                        <td>{{ $item->sku }}</td>
                                        <td>{{ $item->quantity }}</td>
                                        <td>{{ $item->color }}</td>
                                        <td>{{ $item->size }}</td>
                                        <td>{{ \App\Models\Product::convertToUsDollars($item->price_cents) }}</td>
                                        <td>{{ \App\Models\Product::convertToUsDollars($item->cost_cents) }}</td>
                                        <td>
                                            <div class="row m-1">
                                                <div class="col">
                                                    <a href="{{ route('dashboard.inventory.edit', ['inventory' => $item->id]) }}" class="btn btn-sm btn-warning">Edit</a>
                                                </div>
                                            </div>
                                            <div class="row m-1">
                                                <div class="col">
                                                    <form class="row" method="POST" action="{{ route('dashboard.inventory.delete', ['inventory' => $item->id]) }}" class="">
                                                        @csrf
                                                        {{method_field('DELETE')}}
                                                        <div class="col-2">
                                                            <input type="hidden" name="inventory_id" value="{{ $item->id }}" />
                                                            <button type="submit" class="btn btn-sm btn-danger">Delete</button>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            <nav class="pagination" aria-label="Page navigation example">
                                <ul class="pagination">
                                    <li class="page-item {{ ($inventory->currentPage() == 1) ? ' disabled' : '' }}">
                                        @if($inventory->currentPage() == 1)
                                            <a href="#" onclick="return false;" class="page-link">Previous</a>
                                        @else
                                            <a href="{{ $inventory->url(1) }}" class="page-link">Previous</a>
                                        @endif
                                    </li>
                                    @for ($i = 1; $i <= $inventory->lastPage(); $i++)
                                        <li class="page-item {{ ($inventory->currentPage() == $i) ? ' active' : '' }}">
                                            <a href="{{ route('dashboard.inventory.home', ['page' => $i]) }}" class="page-link">{{ $i }}</a>
                                        </li>
                                    @endfor
                                    <li class="page-item {{ ($inventory->currentPage() == $inventory->lastPage()) ? ' disabled' : '' }}">
                                        @if($inventory->currentPage() == $inventory->lastPage())
                                            <a href="#" onclick="return false;" class="page-link">Next</a>
                                        @else
                                            <a href="{{ $inventory->url($inventory->currentPage()+1) }}" class="page-link" >Next</a>
                                        @endif
                                    </li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </x-slot>
    <x-slot name="scripts">
        <script>
            $(document).ready(function(){

                // On Load
                updateRangeCurrentValue();

                $('input#quantity_limit').change(function(){
                    updateRangeCurrentValue()
                });

            });

            function updateRangeCurrentValue()
            {

                var value = $('input#quantity_limit').val();

                $('span#qty-slider-value').html(value)

            }
        </script>
    </x-slot>
</x-bootstrap-app-layout>
