# About this Project

This project was put together as a submittion for a technical coding exam. The project is PHP based using the Laravel 8.0 framework.

## Installation
Installation assumes you have some form of the Docker engine installed to deploy this project.

1. Download this codebase.
1. Copy the .env.example to .env
1. Build docker containers `docker-compose up --build`
1. Install composer dependencies. Docker `app` service has a containerized composer installed: `docker-compose exec app composer install`
1. Generate an **APP_KEY** for Laravel: `docker-compose exec app php artisan key:generate`
1. Copy the CSV data files provided by interviewer to `storage/data`. Directory should look like so:
    ```text
   - routes
   - storage
   -- data
   ---- inventory.csv
   ---- orders.csv
   ---- products.csv
   ---- users.csv
   -- public
    ```
1. Inflate the data by running the provided artisan command: `docker-compose exec app php artisan inflate:data`. Note depending on system resources and Docker allocation this may take a 10 or so minutes to run.
1. Visit the project from the front-end: `http://localhost:8021`. 8021 refers to the port that is exposed from the Docker host to the Docker container. If you changed `APP_PORT` in your .env, use that value.

## Remote viewing
During the interview process this project will be hosted out of [https://cs-fs-06-2021.daniel-paz.com/](https://cs-fs-06-2021.daniel-paz.com/).
