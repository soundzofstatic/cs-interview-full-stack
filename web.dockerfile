FROM nginx:1.10

RUN apt-get update && \
    apt-get dist-upgrade -y && \
    apt-get install --no-install-recommends -y apache2-utils

ADD vhost.conf /etc/nginx/conf.d/default.conf
