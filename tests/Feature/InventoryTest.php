<?php

namespace Tests\Feature;

use App\Models\Inventory;
use App\Models\Product;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class InventoryTest extends TestCase
{
    use RefreshDatabase;

    public function test_authenticated_user_can_see_inventory()
    {

        $this->post(route('register'), [
            'name' => 'Test Man',
            'email' => 'Test@example.com',
            'shop_name' => 'Some Test Shop',
            'shop_domain' => 'sometestshop',
            'password' => 'localdev',
            'password_confirmation' => 'localdev'
        ]);

        $this->assertCount(1, $users = User::all());
        $this->assertAuthenticatedAs($user = $users->first());

        $this->assertCount(0,  Inventory::all());
        $response = $this->actingAs($user)->get(route('dashboard.inventory.home'));

        $response->assertSessionDoesntHaveErrors();
        $response->assertStatus(200);
        $this->assertCount(0,  Inventory::all());

    }

    public function test_authenticated_user_can_search_for_specific_product_id()
    {

        $this->post(route('register'), [
            'name' => 'Test Man',
            'email' => 'Test@example.com',
            'shop_name' => 'Some Test Shop',
            'shop_domain' => 'sometestshop',
            'password' => 'localdev',
            'password_confirmation' => 'localdev'
        ]);

        $this->assertCount(1, $users = User::all());
        $this->assertAuthenticatedAs($user = $users->first());

        $product = new Product();
        $product->id = 1;
        $product->name = 'Preppy Bra';
        $product->description = 'Preppy Bra is an amazing Bra Product...Lorem ipsum dolor sit amet, ne est diceret platonem, ius viris fabulas ea. Amet elit putent ea sit, per accumsan verterem cu.';
        $product->style = 'Preppy';
        $product->brand = 'Panache';
        $product->url = null;
        $product->type = 'clothing';
        $product->shipping_price = 953;
        $product->note = null;
        $product->admin_id = $user->id;
        $product->save();

        // Product belonging to someoneelse
        $product2 = new Product();
        $product2->id = 2;
        $product2->name = 'Another Preppy Bra';
        $product2->description = 'Preppy Bra is an amazing Bra Product...Lorem ipsum dolor sit amet, ne est diceret platonem, ius viris fabulas ea. Amet elit putent ea sit, per accumsan verterem cu.';
        $product2->style = 'Preppy';
        $product2->brand = 'Panache';
        $product2->url = null;
        $product2->type = 'clothing';
        $product2->shipping_price = 900;
        $product2->note = null;
        $product2->admin_id = 2;
        $product2->save();

        $inventory = new Inventory();
        $inventory->product_id = $product->id;
        $inventory->quantity = 70;
        $inventory->color = 'Blue';
        $inventory->size = 'L';
        $inventory->weight = 5.67;
        $inventory->price_cents = 867;
        $inventory->sale_price_cents = 812;
        $inventory->cost_cents = 412;
        $inventory->sku = 'ABCDE';
        $inventory->length = 4.00;
        $inventory->width = 4.00;
        $inventory->height = 2.00;
        $inventory->note = null;
        $inventory->save();

        $inventory2 = new Inventory();
        $inventory2->product_id = $product->id;
        $inventory2->quantity = 95;
        $inventory2->color = 'Black';
        $inventory2->size = 'L';
        $inventory2->weight = 5.67;
        $inventory2->price_cents = 867;
        $inventory2->sale_price_cents = 812;
        $inventory2->cost_cents = 412;
        $inventory2->sku = 'FGHIJ';
        $inventory2->length = 4.00;
        $inventory2->width = 4.00;
        $inventory2->height = 2.00;
        $inventory2->note = null;
        $inventory2->save();

        $inventory3 = new Inventory();
        $inventory3->product_id = $product2->id;
        $inventory3->quantity = 82;
        $inventory3->color = 'Yellow';
        $inventory3->size = 'S';
        $inventory3->weight = 5.67;
        $inventory3->price_cents = 1211;
        $inventory3->sale_price_cents = 999;
        $inventory3->cost_cents = 412;
        $inventory3->sku = 'KLMNOP';
        $inventory3->length = 4.00;
        $inventory3->width = 4.00;
        $inventory3->height = 2.00;
        $inventory3->note = null;
        $inventory3->save();

        $this->assertCount(3,  Inventory::all());
        $response = $this->actingAs($user)->post(route('dashboard.inventory.search'), [
            'query' => '1',
            'quantity_limit' => 100,
        ]);

        $response->assertSessionDoesntHaveErrors();
        $response->assertStatus(200);
        $response->assertSessionHas('query');
        $response->assertSessionHas('quantity_limit');
        $this->assertCount(3,  Inventory::all());
        $this->assertEquals(2, $response['inventory']->count());

    }

    public function test_authenticated_user_can_search_for_specific_inventory_sku()
    {

        $this->post(route('register'), [
            'name' => 'Test Man',
            'email' => 'Test@example.com',
            'shop_name' => 'Some Test Shop',
            'shop_domain' => 'sometestshop',
            'password' => 'localdev',
            'password_confirmation' => 'localdev'
        ]);

        $this->assertCount(1, $users = User::all());
        $this->assertAuthenticatedAs($user = $users->first());

        $product = new Product();
        $product->id = 1;
        $product->name = 'Preppy Bra';
        $product->description = 'Preppy Bra is an amazing Bra Product...Lorem ipsum dolor sit amet, ne est diceret platonem, ius viris fabulas ea. Amet elit putent ea sit, per accumsan verterem cu.';
        $product->style = 'Preppy';
        $product->brand = 'Panache';
        $product->url = null;
        $product->type = 'clothing';
        $product->shipping_price = 953;
        $product->note = null;
        $product->admin_id = $user->id;
        $product->save();

        // Product belonging to someoneelse
        $product2 = new Product();
        $product2->id = 2;
        $product2->name = 'Another Preppy Bra';
        $product2->description = 'Preppy Bra is an amazing Bra Product...Lorem ipsum dolor sit amet, ne est diceret platonem, ius viris fabulas ea. Amet elit putent ea sit, per accumsan verterem cu.';
        $product2->style = 'Preppy';
        $product2->brand = 'Panache';
        $product2->url = null;
        $product2->type = 'clothing';
        $product2->shipping_price = 900;
        $product2->note = null;
        $product2->admin_id = 2;
        $product2->save();

        $inventory = new Inventory();
        $inventory->product_id = $product->id;
        $inventory->quantity = 70;
        $inventory->color = 'Blue';
        $inventory->size = 'L';
        $inventory->weight = 5.67;
        $inventory->price_cents = 867;
        $inventory->sale_price_cents = 812;
        $inventory->cost_cents = 412;
        $inventory->sku = 'ABCDE';
        $inventory->length = 4.00;
        $inventory->width = 4.00;
        $inventory->height = 2.00;
        $inventory->note = null;
        $inventory->save();

        $inventory2 = new Inventory();
        $inventory2->product_id = $product->id;
        $inventory2->quantity = 95;
        $inventory2->color = 'Black';
        $inventory2->size = 'L';
        $inventory2->weight = 5.67;
        $inventory2->price_cents = 867;
        $inventory2->sale_price_cents = 812;
        $inventory2->cost_cents = 412;
        $inventory2->sku = 'FGHIJ';
        $inventory2->length = 4.00;
        $inventory2->width = 4.00;
        $inventory2->height = 2.00;
        $inventory2->note = null;
        $inventory2->save();

        $inventory3 = new Inventory();
        $inventory3->product_id = $product2->id;
        $inventory3->quantity = 82;
        $inventory3->color = 'Yellow';
        $inventory3->size = 'S';
        $inventory3->weight = 5.67;
        $inventory3->price_cents = 1211;
        $inventory3->sale_price_cents = 999;
        $inventory3->cost_cents = 412;
        $inventory3->sku = 'KLMNOP';
        $inventory3->length = 4.00;
        $inventory3->width = 4.00;
        $inventory3->height = 2.00;
        $inventory3->note = null;
        $inventory3->save();

        $this->assertCount(3,  Inventory::all());
        $response = $this->actingAs($user)->post(route('dashboard.inventory.search'), [
            'query' => 'ABCDE',
            'quantity_limit' => 100,
        ]);

        $response->assertSessionDoesntHaveErrors();
        $response->assertStatus(200);
        $response->assertSessionHas('query');
        $response->assertSessionHas('quantity_limit');
        $this->assertCount(3,  Inventory::all());
        $this->assertEquals(1, $response['inventory']->count());

    }

    public function test_authenticated_user_can_reset_search()
    {

        $this->post(route('register'), [
            'name' => 'Test Man',
            'email' => 'Test@example.com',
            'shop_name' => 'Some Test Shop',
            'shop_domain' => 'sometestshop',
            'password' => 'localdev',
            'password_confirmation' => 'localdev'
        ]);

        $this->assertCount(1, $users = User::all());
        $this->assertAuthenticatedAs($user = $users->first());

        $product = new Product();
        $product->id = 1;
        $product->name = 'Preppy Bra';
        $product->description = 'Preppy Bra is an amazing Bra Product...Lorem ipsum dolor sit amet, ne est diceret platonem, ius viris fabulas ea. Amet elit putent ea sit, per accumsan verterem cu.';
        $product->style = 'Preppy';
        $product->brand = 'Panache';
        $product->url = null;
        $product->type = 'clothing';
        $product->shipping_price = 953;
        $product->note = null;
        $product->admin_id = $user->id;
        $product->save();

        // Product belonging to someoneelse
        $product2 = new Product();
        $product2->id = 2;
        $product2->name = 'Another Preppy Bra';
        $product2->description = 'Preppy Bra is an amazing Bra Product...Lorem ipsum dolor sit amet, ne est diceret platonem, ius viris fabulas ea. Amet elit putent ea sit, per accumsan verterem cu.';
        $product2->style = 'Preppy';
        $product2->brand = 'Panache';
        $product2->url = null;
        $product2->type = 'clothing';
        $product2->shipping_price = 900;
        $product2->note = null;
        $product2->admin_id = 2;
        $product2->save();

        $inventory = new Inventory();
        $inventory->product_id = $product->id;
        $inventory->quantity = 70;
        $inventory->color = 'Blue';
        $inventory->size = 'L';
        $inventory->weight = 5.67;
        $inventory->price_cents = 867;
        $inventory->sale_price_cents = 812;
        $inventory->cost_cents = 412;
        $inventory->sku = 'ABCDE';
        $inventory->length = 4.00;
        $inventory->width = 4.00;
        $inventory->height = 2.00;
        $inventory->note = null;
        $inventory->save();

        $inventory2 = new Inventory();
        $inventory2->product_id = $product->id;
        $inventory2->quantity = 95;
        $inventory2->color = 'Black';
        $inventory2->size = 'L';
        $inventory2->weight = 5.67;
        $inventory2->price_cents = 867;
        $inventory2->sale_price_cents = 812;
        $inventory2->cost_cents = 412;
        $inventory2->sku = 'FGHIJ';
        $inventory2->length = 4.00;
        $inventory2->width = 4.00;
        $inventory2->height = 2.00;
        $inventory2->note = null;
        $inventory2->save();

        $inventory3 = new Inventory();
        $inventory3->product_id = $product2->id;
        $inventory3->quantity = 82;
        $inventory3->color = 'Yellow';
        $inventory3->size = 'S';
        $inventory3->weight = 5.67;
        $inventory3->price_cents = 1211;
        $inventory3->sale_price_cents = 999;
        $inventory3->cost_cents = 412;
        $inventory3->sku = 'KLMNOP';
        $inventory3->length = 4.00;
        $inventory3->width = 4.00;
        $inventory3->height = 2.00;
        $inventory3->note = null;
        $inventory3->save();

        $this->assertCount(3,  Inventory::all());
        $response = $this->actingAs($user)->post(route('dashboard.inventory.search'), [
            'query' => 'ABCDE',
            'quantity_limit' => 100,
        ]);

        $response->assertSessionDoesntHaveErrors();
        $response->assertStatus(200);
        $response->assertSessionHas('query');
        $response->assertSessionHas('quantity_limit');
        $this->assertEquals(1, $response['inventory']->count());

        $this->assertCount(3,  Inventory::all());

        $response = $this->actingAs($user)->get(route('dashboard.inventory.home', ['reset' => 'reset']));

        $response->assertSessionDoesntHaveErrors();
        $response->assertStatus(200);
        $response->assertSessionMissing('query');
        $response->assertSessionMissing('quantity_limit');
        $this->assertEquals(2, $response['inventory']->count());

        $this->assertCount(3,  Inventory::all());

    }

    public function test_authenticated_user_can_delete_inventory()
    {

        $this->post(route('register'), [
            'name' => 'Test Man',
            'email' => 'Test@example.com',
            'shop_name' => 'Some Test Shop',
            'shop_domain' => 'sometestshop',
            'password' => 'localdev',
            'password_confirmation' => 'localdev'
        ]);

        $this->assertCount(1, $users = User::all());
        $this->assertAuthenticatedAs($user = $users->first());

        $product = new Product();
        $product->id = 1;
        $product->name = 'Preppy Bra';
        $product->description = 'Preppy Bra is an amazing Bra Product...Lorem ipsum dolor sit amet, ne est diceret platonem, ius viris fabulas ea. Amet elit putent ea sit, per accumsan verterem cu.';
        $product->style = 'Preppy';
        $product->brand = 'Panache';
        $product->url = null;
        $product->type = 'clothing';
        $product->shipping_price = 953;
        $product->note = null;
        $product->admin_id = $user->id;
        $product->save();

        $inventory = new Inventory();
        $inventory->product_id = $product->id;
        $inventory->quantity = 70;
        $inventory->color = 'Blue';
        $inventory->size = 'L';
        $inventory->weight = 5.67;
        $inventory->price_cents = 867;
        $inventory->sale_price_cents = 812;
        $inventory->cost_cents = 412;
        $inventory->sku = 'ABCDE';
        $inventory->length = 4.00;
        $inventory->width = 4.00;
        $inventory->height = 2.00;
        $inventory->note = null;
        $inventory->save();

        $this->assertCount(1,  Inventory::all());

        $response = $this->actingAs($user)->delete(route('dashboard.inventory.delete', ['inventory' => $inventory->id]), [
            'product_id' => $product->id
        ]);

        $response->assertSessionDoesntHaveErrors();
        $response->assertStatus(302);
        $this->assertCount(0,  Inventory::all());

    }

    public function test_authenticated_user_can_acess_edit_inventory_form()
    {

        $this->post(route('register'), [
            'name' => 'Test Man',
            'email' => 'Test@example.com',
            'shop_name' => 'Some Test Shop',
            'shop_domain' => 'sometestshop',
            'password' => 'localdev',
            'password_confirmation' => 'localdev'
        ]);

        $this->assertCount(1, $users = User::all());
        $this->assertAuthenticatedAs($user = $users->first());

        $product = new Product();
        $product->id = 1;
        $product->name = 'Preppy Bra';
        $product->description = 'Preppy Bra is an amazing Bra Product...Lorem ipsum dolor sit amet, ne est diceret platonem, ius viris fabulas ea. Amet elit putent ea sit, per accumsan verterem cu.';
        $product->style = 'Preppy';
        $product->brand = 'Panache';
        $product->url = null;
        $product->type = 'clothing';
        $product->shipping_price = 953;
        $product->note = null;
        $product->admin_id = $user->id;
        $product->save();

        $inventory = new Inventory();
        $inventory->product_id = $product->id;
        $inventory->quantity = 70;
        $inventory->color = 'Blue';
        $inventory->size = 'L';
        $inventory->weight = 5.67;
        $inventory->price_cents = 867;
        $inventory->sale_price_cents = 812;
        $inventory->cost_cents = 412;
        $inventory->sku = 'ABCDE';
        $inventory->length = 4.00;
        $inventory->width = 4.00;
        $inventory->height = 2.00;
        $inventory->note = null;
        $inventory->save();

        $this->assertCount(1,  Product::all());
        $this->assertCount(1,  Inventory::all());
        $response = $this->actingAs($user)->get(route('dashboard.inventory.edit', ['inventory' => $inventory->id]));

        $response->assertSessionDoesntHaveErrors();
        $response->assertStatus(200);
        $this->assertCount(1,  Product::all());
        $this->assertCount(1,  Inventory::all());

    }

    public function test_authenticated_user_can_update_inventory_record()
    {

        $this->post(route('register'), [
            'name' => 'Test Man',
            'email' => 'Test@example.com',
            'shop_name' => 'Some Test Shop',
            'shop_domain' => 'sometestshop',
            'password' => 'localdev',
            'password_confirmation' => 'localdev'
        ]);

        $this->assertCount(1, $users = User::all());
        $this->assertAuthenticatedAs($user = $users->first());

        $product = new Product();
        $product->id = 1;
        $product->name = 'Preppy Bra';
        $product->description = 'Preppy Bra is an amazing Bra Product...Lorem ipsum dolor sit amet, ne est diceret platonem, ius viris fabulas ea. Amet elit putent ea sit, per accumsan verterem cu.';
        $product->style = 'Preppy';
        $product->brand = 'Panache';
        $product->url = null;
        $product->type = 'clothing';
        $product->shipping_price = 953;
        $product->note = null;
        $product->admin_id = $user->id;
        $product->save();

        $inventory = new Inventory();
        $inventory->product_id = $product->id;
        $inventory->quantity = 70;
        $inventory->color = 'Blue';
        $inventory->size = 'L';
        $inventory->weight = 5.67;
        $inventory->price_cents = 867;
        $inventory->sale_price_cents = 812;
        $inventory->cost_cents = 412;
        $inventory->sku = 'ABCDE';
        $inventory->length = 4.00;
        $inventory->width = 4.00;
        $inventory->height = 2.00;
        $inventory->note = null;
        $inventory->save();

        $this->assertCount(1,  Product::all());
        $this->assertCount(1,  Inventory::all());
        $response = $this->actingAs($user)->put(route('dashboard.inventory.update', ['inventory' => $inventory->id]), [
            'price' => '3.50',
            'sale_price' => '3.50',
            'cost' => '4.12',
            'quantity' => 100,
            'color' => 'Blue',
            'size' => 'L',
            'weight' => 4.00,
            'length' => 4.00,
            'width' => 4.00,
            'height' => 4.00,
        ]);

        $response->assertSessionDoesntHaveErrors();
        $response->assertStatus(302);

        $this->assertCount(1,  Product::all());
        $this->assertCount(1,  $allInventory = Inventory::all());
        $inventory = $allInventory->first();
        $response->assertRedirect(route('dashboard.inventory.home'));
        $this->assertEquals(100, $inventory->quantity);
        $this->assertEquals(350, $inventory->sale_price_cents);
        $this->assertEquals(412, $inventory->cost_cents);

    }

}
