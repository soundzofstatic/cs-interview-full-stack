<?php

namespace Tests\Feature;

use App\Models\Inventory;
use App\Models\Product;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class ProductsTest extends TestCase
{
    use RefreshDatabase;

    public function test_authenticated_user_can_see_products()
    {

        $this->post(route('register'), [
            'name' => 'Test Man',
            'email' => 'Test@example.com',
            'shop_name' => 'Some Test Shop',
            'shop_domain' => 'sometestshop',
            'password' => 'localdev',
            'password_confirmation' => 'localdev'
        ]);

        $this->assertCount(1, $users = User::all());
        $this->assertAuthenticatedAs($user = $users->first());

        $this->assertCount(0,  Product::all());
        $response = $this->actingAs($user)->get(route('dashboard.product.home'));

        $response->assertSessionDoesntHaveErrors();
        $response->assertStatus(200);
        $this->assertCount(0,  Product::all());

    }

    public function test_authenticated_user_can_delete_product()
    {

        $this->post(route('register'), [
            'name' => 'Test Man',
            'email' => 'Test@example.com',
            'shop_name' => 'Some Test Shop',
            'shop_domain' => 'sometestshop',
            'password' => 'localdev',
            'password_confirmation' => 'localdev'
        ]);

        $this->assertCount(1, $users = User::all());
        $this->assertAuthenticatedAs($user = $users->first());

        $product = new Product();
        $product->id = 1;
        $product->name = 'Preppy Bra';
        $product->description = 'Preppy Bra is an amazing Bra Product...Lorem ipsum dolor sit amet, ne est diceret platonem, ius viris fabulas ea. Amet elit putent ea sit, per accumsan verterem cu.';
        $product->style = 'Preppy';
        $product->brand = 'Panache';
        $product->url = null;
        $product->type = 'clothing';
        $product->shipping_price = 953;
        $product->note = null;
        $product->admin_id = $user->id;
        $product->save();

        $this->assertCount(1,  Product::all());

        $response = $this->actingAs($user)->delete(route('dashboard.product.delete', ['product' => $product->id]), [
            'product_id' => $product->id
        ]);

        $response->assertSessionDoesntHaveErrors();
        $response->assertStatus(302);
        $this->assertCount(0,  Product::all());

    }

    public function test_authenticated_user_can_acess_create_product_form()
    {

        $this->post(route('register'), [
            'name' => 'Test Man',
            'email' => 'Test@example.com',
            'shop_name' => 'Some Test Shop',
            'shop_domain' => 'sometestshop',
            'password' => 'localdev',
            'password_confirmation' => 'localdev'
        ]);

        $this->assertCount(1, $users = User::all());
        $this->assertAuthenticatedAs($user = $users->first());

        $this->assertCount(0,  Product::all());
        $response = $this->actingAs($user)->get(route('dashboard.product.create'));

        $response->assertSessionDoesntHaveErrors();
        $response->assertStatus(200);
        $this->assertCount(0,  Product::all());

    }

    public function test_authenticated_user_can_store_product()
    {

        $this->post(route('register'), [
            'name' => 'Test Man',
            'email' => 'Test@example.com',
            'shop_name' => 'Some Test Shop',
            'shop_domain' => 'sometestshop',
            'password' => 'localdev',
            'password_confirmation' => 'localdev'
        ]);

        $this->assertCount(1, $users = User::all());
        $this->assertAuthenticatedAs($user = $users->first());

        $this->assertCount(0,  Product::all());
        $response = $this->actingAs($user)->post(route('dashboard.product.store'), [
            'name' => 'Test Name',
            'description' => 'Test Description',
            'style' => 'Test Style',
            'brand' => 'Test Branch',
            'type' => 'clothing',
            'shipping_price' => 1.23
        ]);

        $response->assertSessionDoesntHaveErrors();
        $response->assertStatus(302);

        $this->assertCount(1,  $products = Product::all());
        $product = $products->first();
        $response->assertRedirect(route('dashboard.product.show', ['product' => $product->id]));


    }

    public function test_authenticated_user_can_acess_edit_product_form()
    {

        $this->post(route('register'), [
            'name' => 'Test Man',
            'email' => 'Test@example.com',
            'shop_name' => 'Some Test Shop',
            'shop_domain' => 'sometestshop',
            'password' => 'localdev',
            'password_confirmation' => 'localdev'
        ]);

        $this->assertCount(1, $users = User::all());
        $this->assertAuthenticatedAs($user = $users->first());

        $product = new Product();
        $product->id = 1;
        $product->name = 'Preppy Bra';
        $product->description = 'Preppy Bra is an amazing Bra Product...Lorem ipsum dolor sit amet, ne est diceret platonem, ius viris fabulas ea. Amet elit putent ea sit, per accumsan verterem cu.';
        $product->style = 'Preppy';
        $product->brand = 'Panache';
        $product->url = null;
        $product->type = 'clothing';
        $product->shipping_price = 953;
        $product->note = null;
        $product->admin_id = $user->id;
        $product->save();

        $this->assertCount(1,  Product::all());
        $response = $this->actingAs($user)->get(route('dashboard.product.edit', ['product' => $product->id]));

        $response->assertSessionDoesntHaveErrors();
        $response->assertStatus(200);
        $this->assertCount(1,  Product::all());

    }

    public function test_authenticated_user_can_update_a_product()
    {

        $this->post(route('register'), [
            'name' => 'Test Man',
            'email' => 'Test@example.com',
            'shop_name' => 'Some Test Shop',
            'shop_domain' => 'sometestshop',
            'password' => 'localdev',
            'password_confirmation' => 'localdev'
        ]);

        $this->assertCount(1, $users = User::all());
        $this->assertAuthenticatedAs($user = $users->first());

        $product = new Product();
        $product->id = 1;
        $product->name = 'Preppy Bra';
        $product->description = 'Preppy Bra is an amazing Bra Product...Lorem ipsum dolor sit amet, ne est diceret platonem, ius viris fabulas ea. Amet elit putent ea sit, per accumsan verterem cu.';
        $product->style = 'Preppy';
        $product->brand = 'Panache';
        $product->url = null;
        $product->type = 'clothing';
        $product->shipping_price = 953;
        $product->note = null;
        $product->admin_id = $user->id;
        $product->save();

        $this->assertCount(1,  Product::all());
        $response = $this->actingAs($user)->put(route('dashboard.product.update', ['product' => $product->id]), [
            'name' => 'Test Name UPDATE',
            'description' => 'Test Description',
            'style' => 'Test Style',
            'brand' => 'Test Branch',
            'type' => 'clothing',
            'shipping_price' => 1.23
        ]);

        $response->assertSessionDoesntHaveErrors();
        $response->assertStatus(302);

        $this->assertCount(1,  $products = Product::all());
        $product = $products->first();
        $response->assertRedirect(route('dashboard.product.show', ['product' => $product->id]));
        $this->assertEquals('Test Name UPDATE', $product->name);


    }

    public function test_authenticated_user_can_acess_create_product_inventory_form()
    {

        $this->post(route('register'), [
            'name' => 'Test Man',
            'email' => 'Test@example.com',
            'shop_name' => 'Some Test Shop',
            'shop_domain' => 'sometestshop',
            'password' => 'localdev',
            'password_confirmation' => 'localdev'
        ]);

        $this->assertCount(1, $users = User::all());
        $this->assertAuthenticatedAs($user = $users->first());

        $product = new Product();
        $product->id = 1;
        $product->name = 'Preppy Bra';
        $product->description = 'Preppy Bra is an amazing Bra Product...Lorem ipsum dolor sit amet, ne est diceret platonem, ius viris fabulas ea. Amet elit putent ea sit, per accumsan verterem cu.';
        $product->style = 'Preppy';
        $product->brand = 'Panache';
        $product->url = null;
        $product->type = 'clothing';
        $product->shipping_price = 953;
        $product->note = null;
        $product->admin_id = $user->id;
        $product->save();

        $this->assertCount(1,  Product::all());
        $response = $this->actingAs($user)->get(route('dashboard.product.create-inventory', ['product' => $product->id]));

        $response->assertSessionDoesntHaveErrors();
        $response->assertStatus(200);
        $this->assertCount(1,  Product::all());
        $this->assertCount(0,  Inventory::all());

    }

    public function test_authenticated_user_can_store_product_inventory()
    {

        $this->post(route('register'), [
            'name' => 'Test Man',
            'email' => 'Test@example.com',
            'shop_name' => 'Some Test Shop',
            'shop_domain' => 'sometestshop',
            'password' => 'localdev',
            'password_confirmation' => 'localdev'
        ]);

        $this->assertCount(1, $users = User::all());
        $this->assertAuthenticatedAs($user = $users->first());

        $product = new Product();
        $product->id = 1;
        $product->name = 'Preppy Bra';
        $product->description = 'Preppy Bra is an amazing Bra Product...Lorem ipsum dolor sit amet, ne est diceret platonem, ius viris fabulas ea. Amet elit putent ea sit, per accumsan verterem cu.';
        $product->style = 'Preppy';
        $product->brand = 'Panache';
        $product->url = null;
        $product->type = 'clothing';
        $product->shipping_price = 953;
        $product->note = null;
        $product->admin_id = $user->id;
        $product->save();

        $this->assertCount(1,  Product::all());
        $this->assertCount(0,  Inventory::all());
        $response = $this->actingAs($user)->post(route('dashboard.product.store-inventory', ['product' => $product->id]), [
            'sku' => 'ABCDEF',
            'price' => '3.50',
            'sale_price' => '3.50',
            'cost' => '4.12',
            'quantity' => 100,
            'color' => 'Blue',
            'size' => 'L',
            'weight' => 4.00,
            'length' => 4.00,
            'width' => 4.00,
            'height' => 4.00,
        ]);

        $response->assertSessionDoesntHaveErrors();
        $response->assertStatus(302);
        $response->assertRedirect(route('dashboard.product.show', ['product' => $product->id]));

        $this->assertCount(1,  Product::all());
        $this->assertCount(1,  $allInventory = Inventory::all());
        $inventory = $allInventory->first();
        $this->assertEquals($product->id, $inventory->product_id);
        $this->assertEquals('ABCDEF', $inventory->sku);

    }

}
