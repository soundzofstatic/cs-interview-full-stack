<?php

namespace Tests\Unit;

use App\Models\Product;
use PHPUnit\Framework\TestCase;

class ProductTest extends TestCase
{

    public function test_cents_to_dollars_method()
    {

        $dollars = Product::convertToUsDollars(0);
        $this->assertEquals('$0.00', $dollars);

        $dollars = Product::convertToUsDollars(100);
        $this->assertEquals('$1.00', $dollars);

        $dollars = Product::convertToUsDollars(10000);
        $this->assertEquals('$100.00', $dollars);

        $dollars = Product::convertToUsDollars(100000);
        $this->assertEquals('$1,000.00', $dollars);

        $dollars = Product::convertToUsDollars(13574684168);
        $this->assertEquals('$135,746,841.68', $dollars);

    }
}
